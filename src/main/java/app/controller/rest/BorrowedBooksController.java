package app.controller.rest;


import app.dto.BorrowedBooksDTO;
import app.model.Books;
import app.model.BorrowedBooks;
import app.model.User;
import app.service.BooksService;
import app.service.BorrowedBooksService;
import app.service.UserService;
import app.single_point_access.ServiceSinglePointAccess;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController

@RequestMapping("/borrowedBooks")

public class BorrowedBooksController {
    private BorrowedBooksService booksService = ServiceSinglePointAccess.getBorrowedBooksService();
    private BooksService bService = ServiceSinglePointAccess.getBooksService();
    private UserService user = ServiceSinglePointAccess.getUserService();

    @GetMapping("/all")
    public ResponseEntity<List<BorrowedBooks>> getAllBorrowedBooks() {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findAll());
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<BorrowedBooks> getBorrowedBooksById(@PathVariable Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<BorrowedBooks> createBorrowedBook(@RequestBody BorrowedBooks book) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.save(book));
    }

    @PostMapping("/borrowBook/{memberId}/{memberName}/{title}/{authorName}/{borrowDate}")
    public ResponseEntity<BorrowedBooks> borrowBook(@PathVariable Integer memberId, @PathVariable String memberName, @PathVariable String title, @PathVariable String authorName, @PathVariable String borrowDate) {

        Books carte = bService.findBookByTitleAndAuthor(title, authorName);
        if (carte.getAvailableCopies() > 0) {
            BorrowedBooks borrowedBooks = new BorrowedBooks();
            borrowedBooks.setBorrowDate(borrowDate);
            borrowedBooks.setBook(carte);
            User user1 = user.findByIdAndName(memberId, memberName);
            borrowedBooks.setMember(user1);
            borrowedBooks.setReturnDate("-");
            carte.setAvailableCopies(carte.getAvailableCopies() - 1);
            bService.update(carte);
            return ResponseEntity.status(HttpStatus.OK).body(booksService.save(borrowedBooks));
        }
        return null;
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Boolean> deleteById(@RequestBody Integer id) {
        BorrowedBooks book = booksService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(booksService.delete(book));
    }

    @PutMapping("/update")
    public ResponseEntity<BorrowedBooks> update(@RequestBody BorrowedBooks book) {
        BorrowedBooks bookFromDB = booksService.findById(book.getId());
        bookFromDB.setBook(book.getBook());
        bookFromDB.setMember(book.getMember());
        bookFromDB.setBorrowDate(book.getBorrowDate());
        bookFromDB.setReturnDate(book.getReturnDate());
        BorrowedBooks bookUpdated = booksService.update(bookFromDB);
        return ResponseEntity.status(HttpStatus.OK).body(bookUpdated);
    }

    @PutMapping("/returnBook/{memberId}/{memberName}/{title}/{authorName}/{returnDate}")
    public ResponseEntity<BorrowedBooks> returnBook(@PathVariable Integer memberId, @PathVariable String memberName, @PathVariable String title, @PathVariable String authorName, @PathVariable String returnDate) {
        BorrowedBooks rent = booksService.findBorrowedBook(memberId, memberName, title, authorName);
        rent.setReturnDate(returnDate);
        Books carte = bService.findBookByTitleAndAuthor(title, authorName);
        carte.setAvailableCopies(carte.getAvailableCopies() + 1);
        bService.update(carte);
        return ResponseEntity.status(HttpStatus.OK).body(booksService.update(rent));
    }


    @GetMapping("/getBorrowedBook/{memberId}/{memberName}/{title}/{authorName}")
    public ResponseEntity<BorrowedBooks> findBorrowedBook(@PathVariable Integer memberId, @PathVariable String memberName, @PathVariable String title, @PathVariable String authorName) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findBorrowedBook(memberId, memberName, title, authorName));
    }

    @Operation(summary = "Get details (member, book, borrowDate) from all borrowedbooks")
    @GetMapping("/toBeReturned")
    public ResponseEntity<List<BorrowedBooksDTO>> toBeReturned() {

        List<BorrowedBooks> books = booksService.findAll();
        List<BorrowedBooksDTO> bookDTOS = new ArrayList<>();

        for (BorrowedBooks books1 : books) {
            if (books1.getReturnDate().equals("-")) {
                BorrowedBooksDTO dto = new BorrowedBooksDTO();
                dto.setMember(books1.getMember());
                dto.setBook(books1.getBook());
                dto.setBorrowDate(books1.getBorrowDate());
                bookDTOS.add(dto);
            }
        }

        return ResponseEntity.status(HttpStatus.OK).body(bookDTOS);
    }

}