package app.controller.rest;


import app.model.Authors;
import app.service.AuthorsService;
import app.single_point_access.ServiceSinglePointAccess;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("/authors")

public class AuthorsController {
    private AuthorsService authorsService = ServiceSinglePointAccess.getAuthorsService();

    @GetMapping("/all")
    public ResponseEntity<List<Authors>> getAllAuthors() {
        return ResponseEntity.status(HttpStatus.OK).body(authorsService.findAll());
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Authors> getAuthorById(@PathVariable Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(authorsService.findById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<Authors> createAuthors(@RequestBody Authors authors) {
        return ResponseEntity.status(HttpStatus.OK).body(authorsService.save(authors));
    }

    @PutMapping("/update")
    public ResponseEntity<Authors> update(@RequestBody Authors authors) {
        Authors authorFromDB = authorsService.findById(authors.getId());
        authorFromDB.setName(authors.getName());
        Authors authorUpdated = authorsService.update(authorFromDB);
        return ResponseEntity.status(HttpStatus.OK).body(authorUpdated);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Boolean> deleteById(@RequestBody Integer id) {
        Authors authors = authorsService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(authorsService.delete(authors));
    }

    @GetMapping("/getAuthorByName/{name}")
    public ResponseEntity<Authors> getAuthorByName(@PathVariable String name) {
        Authors authors = authorsService.findAuthorByName((String) name);
        return ResponseEntity.status(HttpStatus.OK).body(authors);
    }

}