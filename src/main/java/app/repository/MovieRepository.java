package app.repository;

import app.model.Movie;

public interface MovieRepository extends CRUDRepository<Movie, Integer>{
}
