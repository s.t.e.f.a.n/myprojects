package app.repository;

import app.model.BorrowedBooks;

import java.util.Date;

public interface BorrowedBooksRepository extends CRUDRepository<BorrowedBooks,Integer> {

    BorrowedBooks borrowBook(Integer memberId, String memberName, String title, String authorName, String borrowDate);

    BorrowedBooks findBorrowedBook(Integer memberId, String memberName, String title, String authorName);

    BorrowedBooks returnBook(Integer memberId, String memberName, String title, String authorName, String returnDate);

}