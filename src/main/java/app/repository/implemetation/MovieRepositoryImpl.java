package app.repository.implemetation;

import app.configuration.HibernateConfiguration;
import app.model.Movie;
import app.repository.MovieRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

public class MovieRepositoryImpl implements MovieRepository {
    @Override
    public Movie save(Movie entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer idOnCarSaved = (Integer) session.save(entity);

        transaction.commit();
        session.close();

        return findById(idOnCarSaved);
    }

    @Override
    public Movie update(Movie entity) {
        // TO DO
        return null;
    }

    @Override
    public Movie findById(Integer id) {
        // TO DO
        return null;
    }

    @Override
    public List<Movie> findAll() {
        // TO DO
        return null;
    }

    @Override
    public boolean delete(Movie entity) {
        // TO DO
        return false;
    }
}
