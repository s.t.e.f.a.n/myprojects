package app.repository.implemetation;

import app.configuration.HibernateConfiguration;
import app.model.Authors;
import app.model.Genres;
import app.repository.GenresRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class GenresRepositoryImpl implements GenresRepository {
    @Override
    public Genres save(Genres entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = (Integer) session.save(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public Genres update(Genres entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.saveOrUpdate(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public boolean delete(Genres entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.delete(entity);

        transaction.commit();
        session.close();

        return findById(id) == null;
    }

    @Override
    public Genres findById(Integer id) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Genres where id=:id");
        query.setParameter("id", id);

        Genres genres;

        try {
            genres = (Genres) query.getSingleResult();
        } catch (NoResultException e) {
            genres = null;
        }

        transaction.commit();
        session.close();

        return genres;
    }

    @Override
    public Genres findGenreByName(String name) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        TypedQuery<Genres> query = session.getNamedQuery("findGenreByName");
        query.setParameter("genreName", name);
        Genres genres;
        try {
            genres = (Genres) query.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
            genres = null;
        }
        transaction.commit();
        session.close();

        return genres;

    }

    @Override
    public List<Genres> findAll() {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        // Native SQL - not preferred
//         Query query = session.createSQLQuery("select * from user");

        TypedQuery<Genres> query = session.getNamedQuery("findAllGenres");
        List<Genres> genres = query.getResultList();

        transaction.commit();
        session.close();

        return genres;
    }

}