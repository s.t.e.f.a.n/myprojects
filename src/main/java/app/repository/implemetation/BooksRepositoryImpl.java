package app.repository.implemetation;

import app.configuration.HibernateConfiguration;
import app.model.Authors;
import app.model.Books;
import app.model.Genres;
import app.repository.BooksRepository;
import app.service.AuthorsService;
import app.service.GenresService;
import app.service.implementation.AuthorsServiceImpl;
import app.single_point_access.ServiceSinglePointAccess;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class BooksRepositoryImpl implements BooksRepository {
    AuthorsService authorsService = new AuthorsServiceImpl();

    @Override
    public Books save(Books entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = (Integer) session.save(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public Books addBook(String title, String authorName, String genresName, String isbn, Integer nrCopii) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = (Integer) session.save(findBookByTitle(title));

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public Books update(Books entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.saveOrUpdate(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }


    @Override
    public boolean delete(Books entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.delete(entity);

        transaction.commit();
        session.close();

        return findById(id) == null;
    }

    @Override
    public boolean removeBook(String title, String authorName) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = findBookByTitleAndAuthor(title, authorName).getId();
        session.delete(findBookByTitleAndAuthor(title, authorName));

        transaction.commit();
        session.close();

        return findById(id) == null;
    }

    @Override
    public Books findById(Integer id) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Books where id=:id");
        query.setParameter("id", id);

        Books books;

        try {
            books = (Books) query.getSingleResult();
        } catch (NoResultException e) {
            books = null;
        }

        transaction.commit();
        session.close();

        return books;
    }

    @Override
    public List<Books> findAll() {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        // Native SQL - not preferred
//         Query query = session.createSQLQuery("select * from user");

        TypedQuery<Books> query = session.getNamedQuery("findAllBooks");
        List<Books> books = query.getResultList();

        transaction.commit();
        session.close();

        return books;
    }


    @Override
    public Books findBookByTitle(String name) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        TypedQuery<Books> query = session.getNamedQuery("findBookByTitle");
        query.setParameter("title", name);
        Books books;
        try {
            books = (Books) query.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
            books = null;
        }
        transaction.commit();
        session.close();

        return books;
    }

    @Override
    public Books findBookByTitleAndAuthor(String title, String authorName) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        TypedQuery<Books> query = session.getNamedQuery("findBookByTitleAndAuthor");
        query.setParameter("title", title);
        query.setParameter("author", authorsService.findAuthorByName(authorName));
        Books books;
        try {
            books = (Books) query.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
            books = null;
        }
        transaction.commit();
        session.close();

        return books;
    }



}