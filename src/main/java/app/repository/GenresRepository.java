package app.repository;

import app.model.Genres;

public interface GenresRepository extends CRUDRepository<Genres,Integer> {
    Genres findGenreByName(String name);
}