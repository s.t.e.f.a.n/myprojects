package app.repository;

import app.model.User;

public interface UserRepository extends CRUDRepository<User, Integer> {
    User findByName(String name);

    User findByIdAndName(Integer id, String name);

}