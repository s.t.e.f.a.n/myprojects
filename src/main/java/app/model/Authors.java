package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries(
        {
                @NamedQuery(name = "findAuthorByName", query = "select a from Authors a where a.name=:name"),
                @NamedQuery(name = "findAuthorById", query = "select a from Authors a where a.id=:id"),
                @NamedQuery(name = "findAllAuthors", query = "select a from Authors a")
        }
)

public class Authors {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
}