package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries(
        {
                @NamedQuery(name = "findBorrowedBookById", query = "select a from BorrowedBooks a where a.id=:id"),
                @NamedQuery(name = "findBorrowedBookByBorrowDate", query = "select a from BorrowedBooks a where a.borrowDate=:borrowDate"),
                @NamedQuery(name = "findAllBorrowedBooks", query = "select a from BorrowedBooks a"),
                @NamedQuery(name = "findBorrowedBook", query = "select a from BorrowedBooks a where a.book=:book and a.member=:member")
        }
)

public class BorrowedBooks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

//    @Column
//    private Integer bookId;
//
//    @Column
//    private Integer memberId;

    @ManyToOne
    @JoinColumn(name = "Books_id", referencedColumnName = "id")
    private Books book;

    @ManyToOne
    @JoinColumn(name = "User_id", referencedColumnName = "id")
    private User member;

    @Column
    private String borrowDate;

    @Column
    private String returnDate;

}