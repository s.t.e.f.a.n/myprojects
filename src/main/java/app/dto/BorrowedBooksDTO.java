package app.dto;

import app.model.Books;
import app.model.User;
import lombok.Data;

@Data
public class BorrowedBooksDTO {
    private User member;
    private Books book;
    private String borrowDate;
}