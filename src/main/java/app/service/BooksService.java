package app.service;

import app.model.Authors;
import app.model.Books;

import java.util.List;

public interface BooksService {
    Books save(Books books);

    Books update(Books books);

    List<Books> findAll();

    Books findById(Integer id);

    boolean delete(Books books);

    Books addBook(String title, String authorName, String genresName, String isbn, Integer nrCopii);

    Books findBookByTitle(String name);

    Books findBookByTitleAndAuthor(String title, String authorName);

    boolean removeBook(String title, String authorName);

}