package app.service.implementation;

import app.model.BorrowedBooks;
import app.repository.BorrowedBooksRepository;
import app.service.BorrowedBooksService;
import app.single_point_access.RepositorySinglePointAccess;

import java.util.Date;
import java.util.List;

public class BorrowedBooksServiceImpl implements BorrowedBooksService {

    private BorrowedBooksRepository borrowedBooksRepository = RepositorySinglePointAccess.getBorrowedBooksRepository();

    @Override
    public BorrowedBooks save(BorrowedBooks books) {
        return borrowedBooksRepository.save(books);
    }

    @Override
    public BorrowedBooks update(BorrowedBooks books) {
        return borrowedBooksRepository.update(books);
    }

    @Override
    public List<BorrowedBooks> findAll() {
        return borrowedBooksRepository.findAll();
    }

    @Override
    public BorrowedBooks findById(Integer id) {
        return borrowedBooksRepository.findById(id);
    }

    @Override
    public boolean delete(BorrowedBooks books) {
        return borrowedBooksRepository.delete(books);
    }

    @Override
    public BorrowedBooks findBorrowedBook(Integer memberId, String memberName, String title, String authorName) {
        return borrowedBooksRepository.findBorrowedBook(memberId, memberName, title, authorName);
    }

    @Override
    public BorrowedBooks borrowBook(Integer memberId, String memberName, String title, String authorName, String borrowDate) {
        return borrowedBooksRepository.borrowBook(memberId, memberName, title, authorName, borrowDate);
    }

    @Override
    public BorrowedBooks returnBook(Integer memberId, String memberName, String title, String authorName, String returnDate) {
        return borrowedBooksRepository.returnBook(memberId, memberName, title, authorName, returnDate);
    }

}