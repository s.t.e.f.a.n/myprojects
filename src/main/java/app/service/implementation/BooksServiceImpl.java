package app.service.implementation;

import app.model.Books;
import app.repository.BooksRepository;
import app.service.BooksService;
import app.single_point_access.RepositorySinglePointAccess;

import java.util.List;

public class BooksServiceImpl implements BooksService {
    private BooksRepository booksRepository = RepositorySinglePointAccess.getBooksRepository();


    @Override
    public Books save(Books books) {
        return booksRepository.save(books);
    }

    @Override
    public Books update(Books books) {
        return booksRepository.update(books);
    }

    @Override
    public List<Books> findAll() {
        return booksRepository.findAll();
    }

    @Override
    public Books findById(Integer id) {
        return booksRepository.findById(id);
    }

    @Override
    public boolean delete(Books books) {
        return booksRepository.delete(books);
    }

    @Override
    public Books findBookByTitle(String name) {
        return booksRepository.findBookByTitle(name);
    }

    @Override
    public Books findBookByTitleAndAuthor(String title, String authorName) {
        return booksRepository.findBookByTitleAndAuthor(title, authorName);
    }

    @Override
    public boolean removeBook(String title, String authorName) {
        return booksRepository.removeBook(title, authorName);
    }

    @Override
    public Books addBook(String title, String authorName, String genresName, String isbn, Integer nrCopii) {
        return booksRepository.addBook(title, authorName, genresName, isbn, nrCopii);
    }

}