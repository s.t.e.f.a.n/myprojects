package app.service.implementation;

import app.model.Authors;
import app.repository.AuthorsRepository;
import app.service.AuthorsService;
import app.single_point_access.RepositorySinglePointAccess;

import java.util.List;

public class AuthorsServiceImpl implements AuthorsService {
    private AuthorsRepository authorsRepository = RepositorySinglePointAccess.getAuthorsRepository();

    @Override
    public Authors save(Authors authors) {
        return authorsRepository.save(authors);
    }

    @Override
    public Authors update(Authors authors) {
        return authorsRepository.update(authors);
    }

    @Override
    public List<Authors> findAll() {
        return authorsRepository.findAll();
    }

    @Override
    public Authors findById(Integer id) {
        return authorsRepository.findById(id);
    }

    @Override
    public boolean delete(Authors authors) {
        return authorsRepository.delete(authors);
    }

    @Override
    public Authors findAuthorByName(String name) {
        return authorsRepository.findAuthorByName(name);
    }

}