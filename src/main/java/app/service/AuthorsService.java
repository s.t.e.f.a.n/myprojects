package app.service;

import app.model.Authors;

import java.util.List;

public interface AuthorsService {
    Authors save(Authors authors);

    Authors update(Authors authors);

    List<Authors> findAll();

    Authors findById(Integer id);

    boolean delete(Authors authors);

    Authors findAuthorByName(String name);

}